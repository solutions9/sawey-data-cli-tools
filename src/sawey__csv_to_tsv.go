/* sawey__csv_to_tsv.go
 * by Bobby Jo Sawey
 *
 * A STUPID-INSANELY-FAST CSV TO TSV CONVERTER USING GO'S BINARY MAGIC
 * AND CONCURRENT READING AND WRITING.
 *
 * Okay it's just pretty fast, not like crazy stupid insane fast.
 * Still, pretty fast.
 * Anyways, goals.
 */

package main

/* updating to add concurrency
* */

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"

	//	"strings"
	"sync"

	//	"io"
	"bufio"
	//	"strings"
	//	"strconv"
	//	"time"
)

// take command line arguments
func CmdArgs() {
	fmt.Println("Usage: ./sawey__csv_to_tsv.go <csv_file> <tsv_file>")
	os.Exit(1)

}

func CSVtoTSV() {
	defer w.Done()

	// lazy := flag.Bool("lazy", false, "lazy mode")
	// trailing := flag.Bool("trailing", false, "trailing mode")
	// reuse_record := flag.Bool("reuse_record", false, "reuse record mode")
	// comment_char := flag.String("comment_char", "#", "comment char")
	// delimiter := flag.String("delimiter", ",", "delimiter")
	// quote_char := flag.String("quote_char", "\"", "quote char")
	// flag.Parse()
	// if flag.NArg() != 2 {
	// 	CmdArgs()
	// }

	ConvertInputFileToOutputFile(flag.Arg(0), flag.Arg(1))

}

// open input file
func OpenInputFile(inputFile string) *os.File {
	// to use your mutex here, just use the mutex.Lock() and mutex.Unlock() functions.

	input, err := os.Open(inputFile)
	if err != nil {
		fmt.Println("Error opening input file:", err)
		os.Exit(1)
	}
	return input
}

// open output file
func OpenOutputFile(outputFile string) *os.File {
	// to use your mutex here, just use the mutex.Lock() and mutex.Unlock() functions.

	output, err := os.Create(outputFile)
	if err != nil {
		fmt.Println("Error opening output file:", err)
		os.Exit(1)
	}
	return output
}

/* to use this function do the following:
1.


writer = return_writer(outputFile)
*/
func return_writer(outputFile string) *bufio.Writer {
	// to use your mutex here, just use the mutex.Lock() and mutex.Unlock() functions.

	writer := bufio.NewWriter(OpenOutputFile(outputFile))
	return writer
}

var Counts = 0

// convert input file to output filed
func ConvertInputFileToOutputFile(inputFile string, outputFile string) {

	input := OpenInputFile(inputFile)
	reader := csv.NewReader(input)

	writer := return_writer(outputFile)
	for {
		record, err := reader.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Error reading input file:", err)
			os.Exit(1)
		}
		for _, field := range record {

			writer.WriteString(field)
			writer.WriteString("\t")
		}
		writer.WriteString("\n")

	}

	writer.Flush()
}

var w sync.WaitGroup

func main() {
	w.Add(1)
	// to throttle concurrency, use a mutex
	// the mutex is used to prevent multiple threads from writing to the same file. here is how to use it:
	// mutex.Lock()
	// defer mutex.Unlock()
	// here is where you use the mutex:
	// your function that needs to be protected by the mutex. if you don't use functions then you cannot protect them. refactor your code to use functions.

	// go function to set up flags
	go CSVtoTSV()
	w.Wait()
}
